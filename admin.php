<?php


require 'function/bdd-function.php';
require 'function/article-function.php';
require 'function/utilisateur-function.php';

checkAuthentication();

$bdd = bddConnect();


$articles = getMyArticles($bdd, $_SESSION["utilisateur"]["id"]);

?>

<html>
<head>
    <?php
    include 'parts/global-css.php';
    ?>
</head>
<body>
<div class="container">
    <a href="logout.php">Me déco !</a>
    <h1>Mes articles !</h1>
    <a href="add-article.php">Ajouter un article</a>
    <div class="row">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Titre</th>
                <th scope="col">Apercu de l'image</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
                foreach ($articles as $article){
                    echo(' <tr>
                <th scope="row">'.$article["id"].'</th>
                <td>'.$article["titre"].'</td>
                <td><img style="max-width: 200px" class="img-thumbnail" src="uploads/'.$article["image"].'"></td>
                <td>
                <a href="edit.php?id='.$article["id"].'">Editer</a> <br>
                 <a href="remove-article.php?id='.$article["id"].'">Supprimer</a> <br>
</td>
            </tr>');
                }
            ?>

            </tbody>
        </table>
    </div>
</div>
</body>
</html>
