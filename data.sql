-- MySQL dump 10.13  Distrib 8.0.24, for macos11 (x86_64)
--
-- Host: 127.0.0.1    Database: le_progres
-- ------------------------------------------------------
-- Server version	8.0.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `article` (
                           `id` int NOT NULL AUTO_INCREMENT,
                           `titre` varchar(250) NOT NULL,
                           `type` varchar(250) NOT NULL,
                           `image` varchar(250) DEFAULT NULL,
                           `contenu` text,
                           `id_journaliste` int NOT NULL,
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `article_id_uindex` (`id`),
                           KEY `article_journaliste_id_fk` (`id_journaliste`),
                           CONSTRAINT `article_journaliste_id_fk` FOREIGN KEY (`id_journaliste`) REFERENCES `journaliste` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` (`id`, `titre`, `type`, `image`, `contenu`, `id_journaliste`) VALUES (5,'Clermont Foot 63 en Ligue 1 !!','sports','61ead02d6ce45.jpeg','sdfdsfdsfdsdsfdsfdsfdsf',1);
INSERT INTO `article` (`id`, `titre`, `type`, `image`, `contenu`, `id_journaliste`) VALUES (6,'Clermont a perdu hier','autres','61eacc20bc173.png','On aime le rugby ici de toute facon ',1);
INSERT INTO `article` (`id`, `titre`, `type`, `image`, `contenu`, `id_journaliste`) VALUES (8,'Clermont Foot 63 en Ligue 1 !!','sports','61eacf670660f.png','sdfdsfdsfdsdsfdsfdsfdsf',1);
/*!40000 ALTER TABLE `article` ENABLE KEYS */;

--
-- Table structure for table `journaliste`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `journaliste` (
                               `id` int NOT NULL AUTO_INCREMENT,
                               `nom` varchar(250) NOT NULL,
                               `prenom` varchar(250) NOT NULL,
                               `username` varchar(250) NOT NULL,
                               `password` varchar(250) NOT NULL,
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `journaliste_username_uindex` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `journaliste`
--

/*!40000 ALTER TABLE `journaliste` DISABLE KEYS */;
INSERT INTO `journaliste` (`id`, `nom`, `prenom`, `username`, `password`) VALUES (1,'Delorme','Aurélien','adelorme','$2y$10$QL2RpLCf1zNVBYY87MqNJOpou1Ewo3WIX8Y4Q5myNdmyfprGsqWVq');
INSERT INTO `journaliste` (`id`, `nom`, `prenom`, `username`, `password`) VALUES (2,'Test','Test','test','test');
/*!40000 ALTER TABLE `journaliste` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-21 16:29:41
