<?php
require 'function/bdd-function.php';
require 'function/article-function.php';
    $bdd = bddConnect();


    $articles = getAllArticles($bdd);

?>

<html>
<head>
    <?php
        include 'parts/global-css.php';
    ?>
</head>
<body>
    <div class="container">
        <h1>Les articles du journal !</h1>


        <div class="row">
            <?php
             foreach ($articles as $article){
                 echo('<div class="card '.$article["type"].'" style="width: 18rem;">
                <img class="card-img-top" src="uploads/'.$article["image"].'" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">'.$article["titre"].'</h5>
                    <p class="card-text">'.substr($article["contenu"], 0, 20).' ...</p>
                    <a href="article.php?id='.$article["id"].'" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>');
             }
            ?>
            
        </div>
    </div>
</body>
</html>
