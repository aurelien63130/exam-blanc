<?php
function getUserByUsername($bdd, $username){
    $query = $bdd->prepare('SELECT * FROM journaliste WHERE username = :username');
    $query->execute(["username"=> $username]);
    $resultat = $query->fetch();

    return $resultat;
}

function checkAuthentication(){
    if(!isset($_SESSION["utilisateur"])){
        header("Location: login.php");
    }
}