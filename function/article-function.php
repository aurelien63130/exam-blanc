<?php
    function getAllArticles($bdd){
        $query = $bdd->query('SELECT * FROM article');
        $resultat = $query->fetchAll();
        return $resultat;
    }

function getMyArticles($bdd, $id){
    $query = $bdd->prepare('SELECT * FROM article WHERE id_journaliste = :id ');
    $query->execute(["id"=> $id]);
    $resultat = $query->fetchAll();

    return $resultat;
}

function getOneArticle($bdd, $id){
    $query = $bdd->prepare('SELECT * FROM article WHERE id = :id');
    $query->execute(['id'=> $id]);

    $resultat = $query->fetch();

    return $resultat;
}

function deleteOne($bdd, $id){
    $query = $bdd->prepare('DELETE FROM article WHERE id = :id');
    $query->execute(['id'=> $id]);
}

?>