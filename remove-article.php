<?php
require "function/utilisateur-function.php";
require "function/article-function.php";
require "function/bdd-function.php";

$bdd = bddConnect();
checkAuthentication();
$id = $_GET["id"];
deleteOne($bdd, $id);
header("Location: admin.php");