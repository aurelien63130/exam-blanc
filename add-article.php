<?php


require 'function/bdd-function.php';
require 'function/article-function.php';
require 'function/utilisateur-function.php';

checkAuthentication();


$types = [
    'politique',
    'faits-divers',
    'sports',
    'autres'
];
$errors = [];

if($_SERVER["REQUEST_METHOD"] == "POST") {
    $bdd = bddConnect();

    $uniqName = null;
    if (empty($_POST["titre"])) {
        $errors[] = "Veuillez saisir un titre";
    }

    if (!in_array($_POST["type"], $types)) {
        $errors[] = "Impossible ce type n'existe pas !";
    }

    if (empty($_POST["contenu"])) {
        $errors[] = "Le contenu est vide";
    }

    if (count($errors) == 0) {
        $allowedExtension = ['image/jpeg', 'image/png'];

        if ($_FILES["picture"]["size"] > 1000000) {
            $errors[] = 'Le fichier est trop lourd !';
        }

        if ($_FILES["picture"]["size"] > 1000000) {
            $errors[] = 'Le fichier est trop lourd !';
        }

        if (!in_array($_FILES["picture"]['type'], $allowedExtension)) {
            $errors[] = 'Que du JPEG OU PNG please ';
        }

        $uniqName = uniqid() . '.' . explode('/', $_FILES["picture"]['type'])[1];

        if (count($errors) == 0) {
            move_uploaded_file($_FILES["picture"]["tmp_name"], 'uploads/' . $uniqName);
        }

        $query =
            $bdd->prepare("INSERT INTO article (titre, type , image, contenu, id_journaliste)
            VALUES (:titre, :type, :image, :contenu, :id_journaliste)");
        $query->execute([
            "titre" => $_POST["titre"],
            "type" => $_POST["type"],
            "image" => $uniqName,
            "contenu" => $_POST["contenu"],
            "id_journaliste" => $_SESSION["utilisateur"]["id"]
        ]);
        header("Location: admin.php");
    }
}



?>

<html>
<head>
    <?php
    include 'parts/global-css.php';
    ?>
</head>
<body>
<div class="container">
    <a href="logout.php">Me déco !</a><br>
    <a href="admin.php">Non enfait retour !</a>

    <h1>Ajouter un article !</h1>

    <form enctype="multipart/form-data" method="post" action="add-article.php">
        <div class="mb-3">
            <label for="titre" class="form-label">Titre</label>
            <input type="text" name="titre"  class="form-control" id="titre" aria-describedby="emailHelp">
        </div>

        <div class="mb-3">
            <label for="type" class="form-label">Type</label>
            <select id="type" name="type" class="form-control">
                <?php
                foreach ($types as $typ){
                    echo('<option value="'.$typ.'">'.$typ.'</option>');
                }

                ?>
            </select>
        </div>

        <div class="mb-3">
            <label for="contenu" class="form-label">Contenu</label>
            <input type="text" name="contenu" class="form-control" id="contenu ">
        </div>


        <div class="mb-3">
            <label for="picture" class="form-label">Image</label>

            <input type="file" name="picture" class="form-control">
        </div>


        <input type="submit" class="btn btn-success">

    </form>

    <?php
    foreach ($errors as $error){
        echo('<div class="alert alert-danger" role="alert">
  '.$error.'
</div>');
    }
    ?>

</div>
</body>
</html>
