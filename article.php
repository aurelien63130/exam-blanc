<?php
    require 'function/bdd-function.php';
    require 'function/article-function.php';

    $idArticle = $_GET["id"];
    $bdd = bddConnect();


    $article = getOneArticle($bdd, $idArticle);


?>

<html>
<head>
    <?php
    include 'parts/global-css.php';
    ?>
</head>
<body>
<div class="container">
    <h1>Le détail de l'article <?php echo($article["titre"]); ?></h1>

    <div class="row">
        <div class="col-md-6">
            <img class="img-thumbnail" alt="image détail" src="uploads/<?php echo($article["image"]); ?>">
        </div>
        <div class="col-md-6"><?php echo($article["contenu"]); ?></div>
    </div>
    <a href="index.php">Revenir aux articles</a>
</div>
</body>
</html>

